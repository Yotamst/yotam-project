# People Removal
A python implementation of Image resizing and a graphic user interface.

![arc](tests/resources/images/arc-de-triomphe/arc-de-triomphe.jpg)
![arc compressed](tests/resources/images/arc-de-triomphe/nowoman.jpg)

## Requirements
* [Python 3.8.2](https://www.python.org/downloads/release/python-382/)
* [Pillow](https://pypi.org/project/Pillow/)
* [numpy](https://pypi.org/project/numpy/)

## Installation
* Clone the project

## Usage
* Graphic user interface
```
    app = Window('GUI')
    app.run()
```

* SeamCarving Object
```
    #create an object
    carver = SeamCarving()
    #upload an image
    carver.load_image(utils.open_image())
    
    #resize image
    updated_image = carver.resize(width=new_width, height=new_height)
    
    #object removal
    updated_image = carver.object_removal(image_mask=image_mask, insertion=True)
    
    utils.save_image(updated_image)

    
```
![car](tests/resources/images/car/car.jpg)
![car compressed](tests/resources/images/car/compressed_forward.png)


## Sources
* Avidan, S. Shamir, A (July 29, 2007). Seam carving for content-aware image resizing
http://www.eng.tau.ac.il/~avidan/papers/imretFinal.pdf
* Avidan, S. Rubinstein, M. Shamir, A (August 1, 2008). Improved Seam Carving for
Video Retargeting: http://www.eng.tau.ac.il/~avidan/papers/vidret.pdf
* Goldman, Paul (May 21, 2015). Ultra-Orthodox Israeli Press Edits Out Female
Lawmakers From Photograph:
https://www.nbcnews.com/news/world/ultra-orthodox-israeli-press-edits-out-female-law
makers-photograph-n362571
* Obergת James (June 24, 2012). Cosmonauts Who Weren’t There:
http://www.jamesoberg.com/vanishing_cosmonauts_with_photos.pdf
* Stanger, Melissa (May 2, 2018). 6 people who were literally erased from history:
https://www.businessinsider.com/people-who-were-erased-from-history-2013-12