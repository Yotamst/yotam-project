# -*- coding: utf-8 -*-
import logging
from src.Application import *


def main():
    level = logging.INFO
    logging.getLogger().setLevel(level)
    master = Window('GUI')
    master.run()



if __name__ == '__main__':
    main()
