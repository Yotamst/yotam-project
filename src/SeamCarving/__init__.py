# -*- coding: utf-8 -*-
import logging

from PIL import Image
from src.SeamCarving.models import *
import numpy as np
import src.SeamCarving.energy_functions as energy_functions

MASK_ENERGY = 100000.0
RED = np.array([255, 0, 0])


class SeamCarving:
    """
    Seam Carving Controller
    """

    def __init__(self, image_path: str = None, image: Image = None):
        """
        :param image_path: path for image
        :param image: PIL Image object
        """
        self.image = image
        self.energy_pixels = None
        self.pixels = None
        self.object_mask = None
        self.gray_pixels = None
        self.removed_seams = None
        self._original_shape = None
        self._energy_func = energy_functions.GRADIENT
        if image_path:
            self.image_path = image_path
            self.image = self._open_image()
        if self.image:
            self.width, self.height = self.image.size
            self.pixels = np.asarray(self.image)
            self.gray_pixels = np.asarray(self.image.convert(models.GRAYSCALE))
            self.energy_pixels = None
            self.energy()

    def load_image(self, image: Image):
        """
        updates self attributes from a new image
        :param image: PIL image object
        """
        if self.image is not None:
            self.object_mask = None
        self.image = image
        self.width, self.height = image.size

        self.pixels = np.asarray(self.image)
        self.gray_pixels = np.asarray(self.image.convert(models.GRAYSCALE))
        self.energy_pixels = None

        self._original_shape = self.gray_pixels.shape

        self.removed_seams = []

    def show_image(self):
        """
        represents [self.image] on a gallery window
        """
        self.image.show()

    def _open_image(self) -> Image:
        """
        opens an image from [self.image_path]
        :return: image as PIL image object
        """
        return Image.open(self.image_path)

    def close_image(self):
        """
        closes the image from editing.
        """
        if self.image is not None:
            self.image.close()

    def remove_seam(self, seam: Seam = None, seam_mask: np.ndarray = None) -> Image:
        """
        removes seam into the image
        :param seam: Seam object
        :param seam_mask: a boolean mask of the seam - seam's pixels are True
        :return: the updated image
        """
        if seam_mask is None and seam is None:
            return None
        if seam is not None and seam_mask is None:
            seam_mask = self.seam_as_mask(seam)
        if seam is not None:
            seam_mask = self.seam_as_mask(seam)

        shape = list(self.pixels.shape)
        shape[1] -= 1

        self.pixels = self.pixels[seam_mask].reshape(tuple(shape))
        self.image = _to_image(self.pixels)

        self.removed_seams.append(seam)

        if self.object_mask is not None:
            self.object_mask = self.object_mask[seam_mask].reshape(shape[0], shape[1])
        self.gray_pixels = self.gray_pixels[seam_mask].reshape(shape[0], shape[1])

        self.width, self.height = self.image.size
        self.energy(object_mask=self.object_mask)
        return self.image

    def insert_seam(self, seam: Seam) -> Image:
        """
        inserts seam into the image
        :param seam: Seam object
        :return: the updated image
        """
        if self.image.mode == 'L':
            self.pixels = _insert_seam_2d(self.pixels, seam)
        else:
            self.pixels = _insert_seam_3d(self.pixels, seam)
        if self.object_mask is not None:
            self.object_mask = _insert_seam_2d(self.object_mask, seam)

        self.gray_pixels = _insert_seam_2d(self.gray_pixels, seam)
        self.image = _to_image(self.pixels)

        self.width, self.height = self.image.size
        self.energy(object_mask=self.object_mask)

        return self.image


    def insert_seams(self, n: int = 1) -> Image:
        """
        inserts n seams into image
        :param n: natural integer
        :return: updated PIL image
        """
        self.removed_seams = []
        temp_image = self.image.copy()
        if self.object_mask is not None:
            temp_mask = self.object_mask.copy()

        for i in range(n):
            logging.info('calculating seam {I}'.format(I=i + 1))
            mask, seam = self.minimal_seam()
            self.remove_seam(seam=seam, seam_mask=mask)

        seams = self.removed_seams
        self.removed_seams = []

        self.load_image(temp_image)
        if self.object_mask is not None:
            self.object_mask = temp_mask

        seams.reverse()

        i = len(seams)
        while i > 0:
            current_seam = seams.pop()
            logging.info('inserting seam {I}'.format(I=i))
            logging.info(current_seam)
            self.insert_seam(current_seam)

            for seam in seams:
                seam.indexes[np.where(seam.indexes >= current_seam.indexes)] += 2

            i -= 1

        return self.image

    def seam_as_mask(self, seam: Seam) -> np.ndarray:
        """
        converts seam object to a boolean mask of the seam
        :param seam: Seam object
        :return: a boolean mask of the seam - seam's pixels are True
        """
        mask = np.ones(self.energy_pixels.shape, dtype=np.bool)

        for h in range(self.height):
            w = seam[h]
            mask[h, w] = False

        return mask

    def minimal_seam(self) -> tuple:
        """
        :return: minimal seam of the image as boolean mask and as Seam object
        """
        min_energy_matrix, backtrack = self.cumulative_energy_pixels()

        # creating seam and mask from the minimal path on backtrack
        seam = Seam(self.height)
        seam_mask = np.ones(shape=self.energy_pixels.shape, dtype=np.bool)
        w = np.argmin(min_energy_matrix[-1])
        seam.energy = min_energy_matrix[self.height - 1, w]

        for h in range(self.height - 1, -1, -1):
            seam_mask[h, w] = False
            seam[h] = w
            w = backtrack[h, w]

        return seam_mask, seam

    def cumulative_energy_pixels(self) -> tuple:
        """
        :return: a cumulative minimum energy M for all possible connected seams for each entry and backtrack of it
        """
        if self.energy_pixels is None:
            self.energy(self.object_mask)

        cumulative_energy_matrix = self.energy_pixels.copy()

        # for later tracking from lowest value in cumulative_energy_matrix[-1] to its seam source
        backtrack = np.zeros_like(cumulative_energy_matrix, dtype=np.int)

        # calculating minimum possible energy of the minimal seam above each pixel
        for h in range(1, self.height):
            for w in range(self.width):
                if w == 0:
                    energy_pixels_above = cumulative_energy_matrix[h - 1, w:w + 2]
                    start_width_index = w
                else:
                    energy_pixels_above = cumulative_energy_matrix[h - 1, w - 1:w + 2]
                    start_width_index = w - 1

                # index of the minimal cumulative energy pixel above current pixel
                min_index = np.argmin(energy_pixels_above) + start_width_index

                backtrack[h, w] = min_index
                min_energy = cumulative_energy_matrix[h - 1, min_index]
                cumulative_energy_matrix[h, w] += min_energy

        return cumulative_energy_matrix, backtrack

    def cumulative_energy_image(self) -> Image:
        """
        :return: energy image of [self.image]
        """
        cumulative_energy_matrix, backtrack = self.cumulative_energy_pixels()
        return _to_image(cumulative_energy_matrix)

    def energy(self, object_mask: np.ndarray = None) -> np.ndarray:
        """
        :return: energy pixels of [self.image]
        """
        energy_func = energy_functions.functions[self._energy_func]
        self.energy_pixels = energy_func(self.gray_pixels)

        if object_mask is not None:
            self.energy_pixels[np.where(object_mask == models.VALUE_FOR_REMOVAL)] *= -MASK_ENERGY * 100
            self.energy_pixels[np.where(object_mask == models.VALUE_FOR_RETAINING)] = MASK_ENERGY
        return self.energy_pixels

    def energy_image(self) -> Image:
        """
        :return: energy image of [self.image]
        """
        if self.energy_pixels is None:
            self.energy()
        return _to_image(self.energy_pixels)

    def set_energy_function(self, function_num: int = 1):
        """
        updates energy function by indexes from energy_functions.functions
        :param function_num: index to energy_functions.functions
        """
        if 0 <= function_num < len(energy_functions.functions):
            self._energy_func = function_num
        logging.info('Energy function is now {}'.format(function_num))

    def resize(self, width: int, height: int, image_mask: Image = None) -> Image:
        """
        given new width and height and optional mask, creates a resized image of [self.image]
        :param image_mask: mask of pixels for retaining and removing
        :param width: new width of [self.image]
        :param height: new height of [self.image]
        :return: resized image of [self.image]
        """
        dwidth = self.width - width
        dheight = self.height - height

        if image_mask:
            image_mask.show()
            self.object_mask = np.asarray(image_mask.convert(models.GRAYSCALE))

        if width < self.width and height < self.height:
            self._resize_optimal_seam_order(dwidth=dwidth, dheight=dheight)
        else:
            if width != self.width:
                self._resize(dwidth)
            if height != self.height:
                self.rotate_90(k=1)
                self._resize(dheight)
                self.rotate_90(k=-1)

        return self.image

    def _resize(self, seams_num: int):
        """
        resizes the width of [self.image]
        :param seams_num: change on the width
        """
        self.energy(object_mask=self.object_mask)
        if seams_num > 0:

            for i in range(seams_num):
                logging.info('removing seam {I}'.format(I=i + 1))
                minimal_mask, seam = self.minimal_seam()
                logging.debug(seam)
                self.remove_seam(seam=seam, seam_mask=minimal_mask)
        else:
            seams_num *= -1

            max_inserting_for_iteration = int(self.width / 2)

            while seams_num >= max_inserting_for_iteration:
                self.insert_seams(max_inserting_for_iteration)
                seams_num -= max_inserting_for_iteration

            if 0 < seams_num <= max_inserting_for_iteration:
                self.insert_seams(seams_num)

        logging.info('Done!')

    def _resize_optimal_seam_order(self, dwidth: int, dheight: int):
        """
        resizing with Optimal Seams-Order
        :param dwidth: change on the width
        :param dheight: change on the height
        """

        while dwidth > 0 and dheight > 0:

            self.energy(object_mask=self.object_mask)
            vertical_mask, vertical_seam = self.minimal_seam()

            self.rotate_90(k=1)

            self.energy(object_mask=self.object_mask)
            horizontal_mask, horizontal_seam = self.minimal_seam()

            if vertical_seam < horizontal_seam:
                self.rotate_90(k=-1)

                logging.info('removing vertical seam {}'.format(dwidth))
                logging.info(vertical_seam)

                self.remove_seam(seam=vertical_seam, seam_mask=vertical_mask)
                dwidth -= 1

            else:
                logging.info('removing horizontal seam {}'.format(dheight))
                logging.info(horizontal_seam)

                self.remove_seam(seam=horizontal_seam, seam_mask=horizontal_mask)
                self.rotate_90(k=-1)
                dheight -= 1

        if dwidth > 0:
            self._resize(dwidth)

        if dheight > 0:
            self.rotate_90(k=1)
            self._resize(dheight)
            self.rotate_90(k=-1)

        return self.image

    def object_removal(self, image_mask: Image = None, insertion: bool = True) -> Image:
        """
        given new width and height, creates a resized image of [self.image]
        :param image_mask:
        :param insertion:
        :return: resized image of [self.image]
        """
        if image_mask:
            image_mask.show()
            self.object_mask = np.asarray(image_mask.convert(models.GRAYSCALE))

            object_height, object_width = self.removal_object_shape()
            proportion_height = object_height / self.height
            proportion_width = object_width / self.width
            rotated = proportion_width > proportion_height

            if rotated:
                self.rotate_90(1)

            self.energy(object_mask=self.object_mask)
            seams_num = 0
            d = 1
            pixels_left = len(self.object_mask[np.where(self.object_mask == models.VALUE_FOR_REMOVAL)])

            while pixels_left > 0 and d != 0:
                logging.debug('{} pixels are left to be removed'.format(pixels_left))
                logging.info('removing seam {I}'.format(I=seams_num + 1))
                minimal_mask, seam = self.minimal_seam()
                logging.debug(seam)
                self.remove_seam(seam=seam, seam_mask=minimal_mask)
                seams_num += 1

                d = pixels_left
                pixels_left = len(self.object_mask[np.where(self.object_mask == models.VALUE_FOR_REMOVAL)])
                d -= pixels_left

            if insertion:
                max_inserting_for_iteration = int(self.width / 2)
                while seams_num >= max_inserting_for_iteration:
                    self.insert_seams(max_inserting_for_iteration)
                    seams_num -= max_inserting_for_iteration
                if 0 < seams_num < max_inserting_for_iteration:
                    self.insert_seams(seams_num)

            if rotated:
                self.rotate_90(-1)

            logging.info('Done!')
            return self.image

    def rotate_90(self, k: int = 1):
        """
        rotates image by 90 degrees
        :param k: 1 -> clockwise, -1 -> counterclockwise
        """
        angle = 90 * k

        self.image = self.image.rotate(angle, expand=True)

        self.pixels = np.rot90(self.pixels, k=k)
        self.gray_pixels = np.rot90(self.gray_pixels, k=k)

        self.object_mask = None if self.object_mask is None else np.rot90(self.object_mask, k=k)
        self.energy_pixels = None if self.energy_pixels is None else np.rot90(self.energy_pixels, k=k)

        self.height, self.width, _ = self.pixels.shape
        logging.debug('The image has been rotated at {} degrees'.format(angle))

    def draw_seam(self, seam: Seam) -> Image:
        """
        draws a seam on the image
        :param seam: boolean mask of seam
        :return: image with colored seam
        """
        pixels = self.pixels.copy()

        for h in range(self.height):
            w = seam[h]
            pixels[h, w] = RED
        return _to_image(pixels)

    def draw_seams(self, original_pixels: np.ndarray) -> Image:
        """
        draws a seam on the image
        :param original_pixels: original pixels of the image
        :return: image with colored seams
        """
        self.adapt_seams_to_original_size()

        for seam in self.removed_seams:
            for h in range(self.height):
                w = seam[h]

                original_pixels[h, w] = RED

        return _to_image(original_pixels)

    def removal_object_shape(self) -> tuple:
        """
        :return: height and width of the marked object for removal
        """
        rows, cols = np.where(self.object_mask == VALUE_FOR_REMOVAL)

        height = 0 if rows is None or len(rows) <= 0 else np.amax(rows) - np.amin(rows) + 1
        width = 0 if cols is None or len(cols) <= 0 else np.amax(cols) - np.amin(cols) + 1

        return height, width

    def retaining_object_shape(self) -> tuple:
        """
        :return: height and width of the marked object for retaining
        """
        rows, cols = np.where(self.object_mask == VALUE_FOR_RETAINING)

        height = 0 if rows is None or len(rows) <= 0 else np.amax(rows) - np.amin(rows) + 1
        width = 0 if cols is None or len(cols) <= 0 else np.amax(cols) - np.amin(cols) + 1

        return height, width

    def adapt_seams_to_original_size(self):
        """
        adapt seams indexes from [self.removed_seams] to the original pixels of the image
        """
        adapted_seams = []

        i = len(self.removed_seams) - 1
        self.removed_seams.reverse()
        while i > 0:
            current_seam = self.removed_seams.pop(0)
            for seam in self.removed_seams:
                current_seam.indexes[np.where(
                    seam.indexes <= current_seam.indexes)] += 1
            i -= 1

            adapted_seams.append(current_seam)

        adapted_seams.append(self.removed_seams.pop(0))

        self.removed_seams = adapted_seams[-1::-1]


def _insert_seam_2d(pixels: np.ndarray, seam: Seam) -> np.ndarray:
    """
    inserts seam into 2d array
    :param pixels: 2d numpy array
    :param seam: Seam object
    :return: updated array
    """
    height, width = pixels.shape
    updated_matrix = np.zeros((height, width + 1))
    for h in range(height):
        w = seam[h]

        if w == 0:
            p = np.average(pixels[h, w:w + 2])

            updated_matrix[h, w] = pixels[h, w]
            updated_matrix[h, w + 1] = p
            updated_matrix[h, w + 1:] = pixels[h, w:]
        else:
            p = np.average(pixels[h, w - 1:w + 1])

            updated_matrix[h, :w] = pixels[h, :w]
            updated_matrix[h, w] = p
            updated_matrix[h, w + 1:] = pixels[h, w:]

    return updated_matrix


def _insert_seam_3d(pixels: np.ndarray, seam: Seam) -> np.ndarray:
    """
    inserts seam into 2d array
    :param pixels: 2d numpy array
    :param seam: Seam object
    :return: updated array
    """
    height, width, color_len = pixels.shape
    updated_pixels = np.zeros((height, width + 1, color_len))

    for h in range(height):
        w = seam[h]

        for c in range(color_len):
            if w == 0:
                p = np.average(pixels[h, w:w + 2, c])

                updated_pixels[h, w, c] = pixels[h, w, c]
                updated_pixels[h, w + 1, c] = p
                updated_pixels[h, w + 1:, c] = pixels[h, w:, c]
            else:
                p = np.average(pixels[h, w - 1:w + 1, c])

                updated_pixels[h, :w, c] = pixels[h, :w, c]
                updated_pixels[h, w, c] = p
                updated_pixels[h, w + 1:, c] = pixels[h, w:, c]

    return updated_pixels


def _to_image(pixels: np.ndarray) -> Image:
    """
    converts numpy pixels into an image
    :param pixels: numpy 2d array
    :return: PIL Image
    """
    return Image.fromarray(pixels.astype(np.uint8)) if pixels is not None else None
