# -*- coding: utf-8 -*-
import numpy as np
from PIL import ImageColor


class Seam:
    """
    represents seam in an image and its total energy
    """

    def __init__(self, length: int):
        """
        :param length: Integer represents length of the seam.
        """
        self.indexes = np.zeros((length)).astype(int)
        self.length = length
        self.energy = 0

    def get(self, i: int, default: int = 0) -> int:
        """
        :param i: index for [self.indexes], represents height in the image.
        :param default: default value in case of IndexError
        :return: index represents width in the image.
        """
        try:
            return self.indexes[i]
        except IndexError as e:
            return default

    def set(self, key: int, value: int):
        """
        set width index given height index
        :param key: index for [self.indexes], represents height in the image
        :param value: index represents width in the image.
        """
        self.indexes[key] = value

    def __len__(self) -> int:
        return self.length

    def __getitem__(self, i: int) -> int:
        """
        :param i: index for [self.indexes], represents height in the image.
        :return: index represents width in the image.
        """
        return self.get(i)

    def __setitem__(self, key: int, value: int):
        """
        set width index given height index
        :param key: index for [self.indexes], represents height in the image
        :param value: index represents width in the image.
        """
        self.set(key, value)

    def __eq__(self, o) -> bool:
        """
        checks equality to other seam according to their energy.
        :param o: other seam
        :return: True if they equal, False in other case.
        """
        return self.energy == o.energy

    def __gt__(self, other) -> bool:
        """
        checks equality to other seam according to their energy.
        :param other: other seam
        :return: True if [self.energy] is higher than [other.energy], False in other case.
        """

        return self.energy.__gt__(other.energy)

    def __lt__(self, other) -> bool:
        """
        checks equality to other seam according to their energy.
        :param other: other seam
        :return: True if [self.energy] is lower than [other.energy], False in other case.
        """
        return self.energy.__lt__(other.energy)

    def __str__(self) -> str:
        """ Return str(self). """
        return super().__str__() + ' <length={L}, energy={E}>'.format(L=self.length, E=self.energy)

    @staticmethod
    def at(seams, index):
        """
        :param seams: list of Seam objects
        :param index: height index
        :return: list of all the seams in [index]
        """
        return [seam[index] for seam in seams]


RETAINING = 1
REMOVAL = 0
BACKGROUND = 2
GRAYSCALE = 'L'

COLORS = {
    RETAINING: 'green',
    REMOVAL: 'red',
    BACKGROUND: 'white'
}

VALUE_FOR_REMOVAL = ImageColor.getcolor(COLORS[REMOVAL], GRAYSCALE)
VALUE_FOR_RETAINING = ImageColor.getcolor(COLORS[RETAINING], GRAYSCALE)
