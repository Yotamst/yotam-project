# -*- coding: utf-8 -*-
import numpy as np
from numpy.fft import fft2, ifft2


def dual_gradient(gray_pixels: np.ndarray) -> np.ndarray:
    """
    :param gray_pixels: np matrix grayscale of an image
    :return: dual gradient energy of an image
    """
    energy_pixels_w, energy_pixels_h = np.gradient(gray_pixels)
    energy_pixels = 2 * (np.abs(energy_pixels_w) + np.abs(energy_pixels_h))
    return energy_pixels


def sobel(gray_pixels: np.ndarray) -> np.ndarray:
    """
    :param gray_pixels: np matrix grayscale of an image
    :return: sobel energy of an image
    """
    im = gray_pixels.astype(np.float64)

    op1 = np.array([[-1, 0, 1],
                    [-2, 0, 2],
                    [-1, 0, 1]])
    op2 = np.array([[-1, -2, -1],
                    [0, 0, 0],
                    [1, 2, 1]])

    kernel_x = np.zeros(im.shape)
    kernel_x[:op1.shape[0], :op1.shape[1]] = op1
    kernel_x = fft2(kernel_x)

    kernel_y = np.zeros(im.shape)
    kernel_y[:op2.shape[0], :op2.shape[1]] = op2
    kernel_y = fft2(kernel_y)

    fim = fft2(im)
    gx = np.real(ifft2(kernel_x * fim)).astype(float)
    gy = np.real(ifft2(kernel_y * fim)).astype(float)

    gradient = np.sqrt(gx ** 2 + gy ** 2)
    return gradient


def forward(gray_pixels: np.ndarray) -> np.ndarray:
    """
    :param gray_pixels: np matrix grayscale of an image
    :return: forward-energy of an image
    """
    h, w = gray_pixels.shape
    im = gray_pixels.astype(np.float64)

    energy = np.zeros(gray_pixels.shape)
    m = np.zeros(gray_pixels.shape)

    U = np.roll(im, 1, axis=0)
    L = np.roll(im, 1, axis=1)
    R = np.roll(im, -1, axis=1)

    cU = np.abs(R - L)
    cL = np.abs(U - L) + cU
    cR = np.abs(U - R) + cU

    for i in range(1, h):
        mU = m[i - 1]
        mL = np.roll(mU, 1)
        mR = np.roll(mU, -1)

        mULR = np.array([mU, mL, mR])
        cULR = np.array([cU[i], cL[i], cR[i]])
        mULR += cULR

        argmins = np.argmin(mULR, axis=0)
        energy[i] = np.choose(argmins, cULR)
    return energy


functions = [dual_gradient, forward, sobel]
GRADIENT = 0
FORWARD = 1
SOBEL = 2
