# -*- coding: utf-8 -*-
import logging

import tkinter
from PIL import Image, ImageTk, ImageDraw, ImageColor
import src.Application.utils as utils
from src.SeamCarving import SeamCarving, energy_functions, models

DRAW = 3
FILL = 4

INFO_LABEL_DEFAULT = 'Status:'


class Window(tkinter.Tk):
    """
    represents tkinter window
    """

    def __init__(self, title: str = ''):
        tkinter.Tk.__init__(self)
        self.menu = None
        self.canvas = None
        self.options = None
        self.title(title)
        self._build()
        self.carving_controller = SeamCarving()

        self.run = self.mainloop

    def _build(self):
        """
        builds the window
        """
        self._build_menu()
        self._build_canvas()
        self._build_options()

    def _build_menu(self):
        """
        creates menu
        """
        self.menu = tkinter.Menu(self)
        self.config(menu=self.menu)
        filemenu = tkinter.Menu(self)
        self.menu.add_cascade(label='File', menu=filemenu)
        filemenu.add_command(label='New', command=self.clear_canvas)
        filemenu.add_command(label='Open', command=self.open_image)
        filemenu.add_command(label='Save Image', command=self.save_image)
        filemenu.add_command(label='Save Energy Image', command=self.save_energy_image)
        filemenu.add_command(label='Save Mask', command=self.save_mask)
        filemenu.add_command(label='Clear Mask', command=self.clear_image)
        filemenu.add_separator()
        filemenu.add_command(label='Exit', command=self.quit)

    def _build_canvas(self):
        """
        creates canvas
        """
        self.canvas = Canvas(self, bg='white', width=300, height=300)
        self.canvas.pack(side=tkinter.LEFT)
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

    def _build_options(self):
        """
        builds compressions options on the window
        """
        self.options = tkinter.Frame(self)
        self.info = tkinter.StringVar(value=INFO_LABEL_DEFAULT)
        self.info_label = tkinter.Label(self.options, textvariable=self.info).grid(row=0)

        self.radius_label = tkinter.Label(self.options, text="Options").grid(row=1)
        self.radio_value = tkinter.IntVar(master=self.options)
        self.radio_color_buttons = [
            tkinter.Radiobutton(self.options, text='Removal', variable=self.radio_value, value=0,
                                command=self.update_color).grid(row=2)
            , tkinter.Radiobutton(self.options, text='Retaining', variable=self.radio_value, value=1,
                                  command=self.update_color).grid(row=3)]
        self.radius_value = tkinter.IntVar(master=self.options)
        self.tool_value = tkinter.IntVar(master=self.options)
        self.tools_buttons = [
            tkinter.Radiobutton(self.options, text='Draw', variable=self.tool_value, value=DRAW,
                                command=self.update_tool).grid(row=4)
            , tkinter.Radiobutton(self.options, text='Fill', variable=self.tool_value, value=FILL,
                                  command=self.update_tool).grid(row=5)]

        self.energy_label = tkinter.Label(self.options, text="Energy Function").grid(row=6)
        self.energy_value = tkinter.IntVar(master=self.options)
        self.energy_buttons = [
            tkinter.Radiobutton(self.options, text='Dual Gradient', variable=self.energy_value,
                                value=energy_functions.GRADIENT,
                                command=self.update_energy_function).grid(row=7)
            , tkinter.Radiobutton(self.options, text='Forward Energy', variable=self.energy_value,
                                  value=energy_functions.FORWARD,
                                  command=self.update_energy_function).grid(row=8)
            , tkinter.Radiobutton(self.options, text='Sobel', variable=self.energy_value, value=energy_functions.SOBEL,
                                  command=self.update_energy_function).grid(row=9)]

        self.radius_label = tkinter.Label(self.options, text="Circle's radius").grid(row=10)
        self.radius_input = tkinter.Entry(self.options, width=3, textvariable=self.radius_value)
        self.radius_input.insert(2, '10')
        self.radius_input.grid(row=10, column=1)
        self.radius_value.trace_add("write", self._update_drawing_radius)

        self.width_input = tkinter.Entry(self.options, width=10)
        self.width_label = tkinter.Label(self.options, text="Change image's width to").grid(row=11)
        self.width_input.insert(10, '0')
        self.width_input.grid(row=11, column=1)
        self.height_input = tkinter.Entry(self.options, width=10)
        self.height_label = tkinter.Label(self.options, text="Change image's height to").grid(row=12)
        self.height_input.insert(10, '0')
        self.height_input.grid(row=12, column=1)

        self.resize_button = tkinter.Button(self.options,
                                            text='Resize',
                                            command=self.resize).grid(row=13,
                                                                      column=0,
                                                                      sticky=tkinter.W,
                                                                      pady=4)
        self.object_removal_buttons = [tkinter.Button(self.options,
                                                      text='Remove Object & Insert',
                                                      command=self.object_removal_with_insertion).grid(row=13,
                                                                                                       column=1,
                                                                                                       sticky=tkinter.W,
                                                                                                       pady=4),
                                       tkinter.Button(self.options,
                                                      text='Remove Object',
                                                      command=self.object_removal).grid(row=13,
                                                                                        column=2,
                                                                                        sticky=tkinter.W,
                                                                                        pady=4)]

        self.options.pack(side=tkinter.TOP)

    def load_image(self, image: Image):
        """
        given an image, loads it to the canvas
        :param image: PIL image
        """
        if image is not None:
            self.canvas.load_image(image)
            width, height = image.size
            self.width_input.delete(0, 'end')
            self.width_input.insert(10, width)
            self.height_input.delete(0, 'end')
            self.height_input.insert(10, height)

    def set_information_label(self, text: str = INFO_LABEL_DEFAULT):
        """
        updates info on the top of the window
        :param text: text to insert
        """
        if text == INFO_LABEL_DEFAULT:
            self.info.set(INFO_LABEL_DEFAULT)
        else:
            self.info.set('{} {}'.format(INFO_LABEL_DEFAULT, text))

    def resize(self):
        """
        compress image to new width, and loads the compressed image to the canvas
        """
        image = self.grab_image()
        if image is not None:
            self.carving_controller.load_image(image=image)

            new_width = self.width_input.get()
            if new_width.isdigit():
                new_width = int(new_width)
            else:
                self.set_information_label('Please change the width to a number.')
                return

            new_height = self.height_input.get()
            if new_height.isdigit():
                new_height = int(new_height)
            else:
                self.set_information_label('Please change the height to a number.')
                return

            if new_width != image.size[0] or new_height != image.size[1]:
                image_mask = self.canvas.mask() if self.canvas.painted else None
                status = False
                try:
                    image = self.carving_controller.resize(width=new_width, height=new_height, image_mask=image_mask)
                    status = image is not None
                except Exception as e:
                    logging.debug(e)
                    ststus = False

                if status:
                    self.set_information_label('The image has been resized successfully :)')
                else:
                    self.set_information_label('An Error has been occurred :(')
                self.load_image(image)

    def object_removal_with_insertion(self):
        """
        removes the selected object on the image, and loads the new image to the canvas
        """
        status = False
        image = None
        try:

            self.carving_controller.load_image(image=self.grab_image())

            image_mask = self.canvas.mask() if self.canvas.painted else None
            image = self.carving_controller.object_removal(image_mask=image_mask, insertion=True)

            status = image is not None

        except Exception as e:
            ststus = False

        if status:
            self.set_information_label('The image has been resized successfully :)')
        else:
            self.set_information_label('An Error has been occurred :(')
        self.load_image(image)

    def object_removal(self):
        """
        removes the selected object on the image, and loads the new image to the canvas
        """
        status = False
        image = None
        try:

            self.carving_controller.load_image(image=self.grab_image())

            image_mask = self.canvas.mask() if self.canvas.painted else None
            image = self.carving_controller.object_removal(image_mask=image_mask, insertion=False)

            status = image is not None

        except Exception as e:
            ststus = False

        if status:
            self.set_information_label('The image has been resized successfully :)')
        else:
            self.set_information_label('An Error has been occurred :(')
        self.load_image(image)

    def open_image(self):
        """
        opens image from file explorer and loads it to the canvas
        """
        img = utils.open_image()
        if img:
            self.load_image(img)

    def grab_image(self) -> Image:
        """
        :return: canvas as an image
        """
        return self.canvas.capture()

    def save_image(self) -> bool:
        """
        saves the canvas as image
        :return: saving status
        """
        image = self.grab_image()
        return utils.save_image(image)

    def save_energy_image(self) -> bool:
        """
        saves the energy image of the canvas
        :return: saving status
        """
        image = self.grab_image()
        self.carving_controller.load_image(image)
        energy_image = self.carving_controller.energy_image()
        return utils.save_image(energy_image)

    def save_mask(self) -> bool:
        """
        saves the mask (drawing) on canvas
        :return: saving status
        """
        mask = self.canvas.mask()
        return utils.save_image(mask)

    def clear_image(self):
        """
        clears the canvas
        """
        self.canvas.clear_image()

    def update_color(self):
        """
        updates canvas drawing color with the chosen value from radio buttons.
        """
        self.canvas.drawing_color = models.COLORS.get(self.radio_value.get(), models.COLORS[models.REMOVAL])

    def update_tool(self):
        """
        updates canvas drawing color with the chosen value from radio buttons.
        """
        self.canvas.tool = self.tool_value.get()
        if self.canvas.tool == DRAW:
            self.canvas.bind("<B1-Motion>", self.canvas.draw)
            self.canvas.bind("<Button-1>", None)
        elif self.canvas.tool == FILL:
            self.canvas.bind("<B1-Motion>", None)
            self.canvas.bind("<Button-1>", self.canvas.fill)

            self.set_information_label('Draw a border with "Draw" and then Click with "Fill" option on it!')

    def update_energy_function(self):
        """
        updates energy function
        """
        energy_function_value = self.energy_value.get()
        self.carving_controller.set_energy_function(energy_function_value)

    def _update_drawing_radius(self, *event):
        """
        updates painted circle's radius
        :param event: event info
        """
        try:
            radius = self.radius_value.get()
            self.canvas.circle_radius = radius
        except Exception as e:
            self.set_information_label('Please use a number')
            logging.debug(e)

    def clear_canvas(self):
        self.canvas.clear()


class Canvas(tkinter.Canvas):
    """
    represents tkinter canvas, uses image as background
    """

    def __init__(self, master: tkinter.Tk = None, width: int = 300, height: int = 250, bg: str = 'white'):
        tkinter.Canvas.__init__(self, master=master, width=width, height=height, bg=bg)
        self._draw_image = None
        self._mask_image = None
        self._normal_image = None
        self.painted = False
        self.circle_radius = 10
        self.drawing_color = models.COLORS[models.REMOVAL]
        self.tool = DRAW

    def draw(self, event: tkinter.Event):
        """
        given mouse event click, draws on the image
        :param event: click event on canvas
        """
        if self.tool == DRAW:
            if self._draw_image:
                self.create_circle(event.x, event.y, self.circle_radius, fill=self.drawing_color,
                                   outline=self.drawing_color)

                self._draw_image.ellipse(
                    (event.x - self.circle_radius, event.y - self.circle_radius, event.x + self.circle_radius,
                     event.y + self.circle_radius),
                    fill=self.drawing_color)

                self.painted = True

    def fill(self, event: tkinter.Event):
        """
        given mouse event click, draws on the image
        :param event: click event on canvas
        """
        if self.tool == FILL:
            if self._draw_image:
                logging.info('Fill event: {}'.format(event))

                ImageDraw.floodfill(image=self._mask_image, xy=(event.x, event.y),
                                    value=ImageColor.getrgb(self.drawing_color),
                                    border=ImageColor.getrgb(self.drawing_color))

                self.painted = True

    def load_image(self, image: Image):
        """
        given an image, loads it on the canvas
        :param image: PIL image
        """
        self._normal_image = image
        self._mask_image = Image.new(mode='RGB', size=image.size, color=models.COLORS[models.BACKGROUND])
        self.image = ImageTk.PhotoImage(image)

        width, height = image.size
        self.config(width=width, height=height)

        self.create_image(0, 0, image=self.image, anchor='nw')
        self._draw_image = ImageDraw.Draw(self._mask_image)

        self.bind("<B1-Motion>", self.draw)

        self.painted = False

    def create_circle(self, x: int, y: int, r: int, **kw):
        """
        Create circle with coordinates
        :param x: x coordinate of circle's center point
        :param y: x coordinate of circle's center point
        :param r: radius of the circle
        :param kw: kw of create_oval
        :return:
        """
        return self.create_oval(x - r, y - r, x + r, y + r, **kw)

    def capture(self) -> Image:
        """
        :return: canvas as image
        """
        return self._normal_image

    def mask(self) -> Image:
        """
        :return: mask of image
        """
        return self._mask_image

    def set_mask(self, mask: Image):
        self._mask_image = mask

    def clear(self):
        """
        deletes everything on the canvas
        """
        self.delete("all")
        self._normal_image = None
        self.image = None
        self._draw_image = None

    def clear_image(self):
        """
        clears drawing on canvas
        """
        if self._normal_image:
            self.load_image(self._normal_image)
