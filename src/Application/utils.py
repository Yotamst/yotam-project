# -*- coding: utf-8 -*-


import tkinter
import tkinter.filedialog
from PIL import Image

FILE_TYPES = [('Image Files (png, jpg, bmp)', ['.png', '.jpeg', '.jpg', '.bmp'])]


def open_image(file_path: str = '') -> Image:
    """
    :param file_path: path for image file
    :return: given path from path or os-file-explorer gui, returns image file
    """
    if not file_path:
        f = tkinter.filedialog.askopenfile(
            filetypes=FILE_TYPES)
        if f:
            file_path = f.name
    if file_path:
        f = open(file_path, 'rb')
        img = Image.open(f)
        return img
    return None


def save_image(image: Image = None) -> bool:
    """
    saves image with os-file-explorer gui
    :param image: PIL image
    :return: saving status
    """
    if image is not None:
        f = tkinter.filedialog.asksaveasfile(mode='wb',
                                             filetypes=FILE_TYPES,
                                             defaultextension=".{f}".format(f=image.format))
        if f:
            image.save(f.name)
            return True
    return False
